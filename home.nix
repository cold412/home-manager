{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "e14";
  home.homeDirectory = "/home/e14";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.
  
  nixpkgs.config.allowUnfreePredicate = _: true;

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
    microcodeAmd
    alacritty
    mullvad-browser
    alsa-utils
    autorandr
    bitwarden
    blueberry
    bluez
    cmus
    calibre
    discord
    feh
    firefox
    freetube
    gnome.gnome-disk-utility
    google-chrome
    gparted
    htop
    keepassxc
    librewolf
    mpv
    neofetch
    networkmanagerapplet
    newsboat
    numlockx
    p7zip
    ranger
    unrar
    redshift
    rtorrent
    signal-desktop
    standardnotes
    unclutter
    vlc
    volumeicon
    xfce.thunar
    xfce.thunar-archive-plugin
    xfce.thunar-media-tags-plugin
    xfce.thunar-volman
    unzip
    nordzy-icon-theme
    nordzy-cursor-theme
    nordic
    buku
    oil-buku
    veracrypt
    zathura
    fsearch
    czkawka
    qalculate-qt
    qbittorrent
    element-desktop
    bemenu
    grim
    slurp
    swappy
    lxqt.pavucontrol-qt
    stellarium
    wl-screenrec
    wf-recorder
    libreoffice
    nicotine-plus
    homebank
    swaylock
    simplex-chat-desktop
    reaper
    tauon
    anytype
    translate-shell
    wpaperd

    (pkgs.writeShellScriptBin "monitorSH_OFF" ''
      #!/usr/bin/env bash
      alacritty -e xset s off;
    '')

    (pkgs.writeShellScriptBin "monitorSH3" ''
      #!/usr/bin/env bash
      alacritty -e xset s 180; 
    '')



    (pkgs.writeShellScriptBin "cmusSH" ''
      #!/usr/bin/env bash
      alacritty -o font.size=9 --class cmus -e cmus; 
    '')

    (pkgs.writeShellScriptBin "rtorrentSH" ''
      #!/usr/bin/env bash
      alacritty -o font.size=9 --class rtorrent -e rtorrent; 
    '')

    (pkgs.writeShellScriptBin "newsboatSH" ''
      #!/usr/bin/env bash
      alacritty -o font.size=9 --class newsboat -e newsboat; 
    '')


    (pkgs.writeShellScriptBin "startHL" ''
      #!/usr/bin/env bash
      tauon &
      alacritty -o font.size=9 --class newsboat -e newsboat &
      LANG=pl signal-desktop &
      simplex-chat-desktop 
    '')

    
  ];




  services.redshift = {
    enable = true;
    provider = "geoclue2";
#    latitude = "55.953251";
#    longitude = "-3.188267";
    tray = true;
  };

  programs.autorandr.enable = true;
  

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
    "/.xinitrc".source = ./.xinitrc; 
    "/.zprofile".source = ./.zprofile; 
    "/.rtorrent.rc".source = ./rtorrent/.rtorrent.rc; 
#    ".config/flameshot/flameshot.ini".source = ./flameshot/flameshot.ini; 
    ".newsboat/config".source = ./newsboat/config; 
#    ".config/awesome/rc.lua".source = ./awesome/rc.lua; 
#    ".config/hypr".source = ./hypr; 
#    ".config/waybar".source = ./hypr/waybar; 
  };


  programs.git = {
    enable = true;
    userName = "cold412";
    userEmail = "cold412+gitlab@protonmail.com";
 };



   programs.zsh = {
     enable = true;
     enableAutosuggestions = true;
     enableCompletion = true;
     oh-my-zsh = {
    enable = true;
    plugins = [ "git" ];
    theme = "amuse";
  };
     initExtra = ''
        neofetch
        export EDITOR='vim'
        export TERMINAL='alacritty'
        export BROWSER='mullvad-browser'
      ''; 
};

  programs.zsh.syntaxHighlighting.enable = true;


  gtk = {
    enable = true;
    theme.package = pkgs.nordic;
    theme.name = "Nordic";
    iconTheme.package = pkgs.nordzy-icon-theme;
    iconTheme.name = "Nordzy-dark";
    font = {
      name = "JetBrainsMono Nerd Font";
      size = 12;
    };
  };



  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/e14/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.

  # Environment
  home.sessionVariables = {
    EDITOR = "vim";
    BROWSER = "mullvad-browser";
    TERMINAL = "alacritty";
  };



  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  xdg.mimeApps = {
    enable = true;
    associations.added = {
      "text/html" = ["mullvad-browser.desktop"];
      "x-scheme-handler/http" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/https" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/about" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/unknown" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/url" = ["mullvadbrowser.desktop"];
      "application/xhtml_xml" = ["mullvadbrowser.desktop"];
      "application/url" = ["mullvadbrowser.desktop"];
    };
    defaultApplications = {
      "text/html" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/http" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/https" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/about" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/unknown" = ["mullvadbrowser.desktop"];
      "x-scheme-handler/url" = ["mullvadbrowser.desktop"];
      "application/xhtml_xml" = ["mullvadbrowser.desktop"];
      "application/url" = ["mullvadbrowser.desktop"];
    };
  };

              nixpkgs.config.permittedInsecurePackages = [
                "electron-25.9.0"
              ];




}
